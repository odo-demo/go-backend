package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-redis/redis/v8"

	"github.com/gin-gonic/gin"
)

var rdb *redis.Client

//CORS middleware
func cors(c *gin.Context) {
	log.Println("enter cors middleware")
	headers := c.Request.Header.Get("Access-Control-Request-Headers")
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Methods", "GET,POST,PUT,HEAD,PATCH,DELETE,OPTIONS")
	c.Header("Access-Control-Allow-Headers", headers)
	if c.Request.Method == "OPTIONS" {
		c.Status(200)
		c.Abort()
	}
	c.Set("start_time", time.Now())
	c.Next()

}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(cors)

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		log.Println("DEFAULT:", os.Getenv("DEFAULT"))
		c.String(http.StatusOK, "pong")
	})

	// Get user value
	r.GET("/stock/:name", func(c *gin.Context) {
		symbol := c.Params.ByName("name")
		value, err := rdb.Get(c, symbol).Result()
		if err == nil {
			c.JSON(http.StatusOK, gin.H{"symbol": symbol, "price": value})
		} else {
			c.JSON(http.StatusOK, gin.H{"symbol": symbol, "price": "no value"})
		}
	})

	/**
	curl -v 127.0.0.1:40001/stock --request PUT -d '{"symbol":"IBM","price":"127.21"}' -H 'content-type: application/json'
	*/
	r.PUT("/stock", func(c *gin.Context) {
		// Parse JSON
		var json struct {
			Symbol string `json:"symbol" binding:"required"`
			Price  string `json:"price" binding:"required"`
		}

		if c.Bind(&json) == nil {
			log.Printf("set stock info %v", json)
			err := rdb.Set(c, json.Symbol, json.Price, 0).Err()
			if err == nil {
				c.JSON(http.StatusOK, gin.H{"status": "ok"})
			} else {
				c.JSON(http.StatusInternalServerError, gin.H{"status": "500"})
			}

		}
	})

	return r
}

func main() {
	redisAddress, ok := os.LookupEnv("DEFAULT")
	if !ok {
		log.Println("Using default redis address")
		rdb = redis.NewClient(&redis.Options{
			Addr: ":6379",
		})
	} else {
		log.Printf("Using  redis address %s \n", redisAddress)
		rdb = redis.NewClient(&redis.Options{
			Addr: redisAddress,
		})
	}

	defer func(rdb *redis.Client) {
		err := rdb.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(rdb)

	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	err := r.Run(":8080")
	if err != nil {
		log.Fatal(err)
	}

}
